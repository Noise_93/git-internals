package core;

import java.io.*;
import java.util.zip.InflaterInputStream;
import java.util.*;
public class GitTreeObject extends GitObject {

	private String path;
	private String masterTreePath;
	private Map<String,String> mapFileToHash=new HashMap<String,String>();
	public GitTreeObject(String repoPath,String masterTreeHash) {
		
		super(repoPath,masterTreeHash);
		path=repoPath;
		masterTreePath=masterTreeHash;
		
	}
	
	public ArrayList<String> getEntryPaths() throws Exception{
		
		int i=-1,j=0,count=0;
		String hash="",filename="";
		ArrayList <String> contentDir=new ArrayList<String>();
		String[] dir= { path, masterTreePath.substring(0,2), masterTreePath.substring(2) };
		String file=path+"/objects/"+dir[1]+"/"+dir[2];
		FileInputStream fis = new FileInputStream(file);
		InflaterInputStream inStream=new InflaterInputStream(fis);
		
		while((i=inStream.read()) != 0){
			//first line
		}
		//content data
		while(((i=inStream.read()) != -1)){
			while(((i=inStream.read()) != 0x20)){
	               		//permission bytes		
			}
			filename="";
			while((i=inStream.read()) != 0){
						
				filename+=(char) i;			
			}
			contentDir.add(filename);

			hash="";
			//read the hash string
			for(j=0;j<20;j++){
			
				i=inStream.read();
				hash+=String.format("%02X",i);
			}
			mapFileToHash.put(filename,hash.toLowerCase());
		}
		return contentDir;
		
	}
	
	public GitObject getEntry(String prova) throws Exception {
		
		GitObject file=new GitObject(path,mapFileToHash.get(prova));
		
		switch(file.getType()) {
		
		case "blob": 
					file= (GitObject) new GitBlobObject(path, mapFileToHash.get(prova));
					break;
		case "tree":
					file= (GitObject) this;
					break;
		default:
					break;
		
		
		}
		return file;
	}
	
	public String getHash() {
		
		return super.getHash();
	}
	
	public String getType() throws Exception{
	
		return super.getType();
		
	}

}
