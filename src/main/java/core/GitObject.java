package core;

import java.io.FileInputStream;
import java.util.zip.InflaterInputStream;

public class GitObject {

	private String hash;
	private String path;
	
	public GitObject(String path,String hash) {
		
		this.path=path;
		this.hash=hash;
		
		
	}
	public String getType() throws Exception{
		

		
		int i;
		char c;
		int count=0;
		String type="";
		String[] dir= { path, hash.substring(0,2), hash.substring(2) };
		String file=path + "/objects/" + dir[1] + "/" + dir[2];
		FileInputStream fis = new FileInputStream(file);
		InflaterInputStream inStream=new InflaterInputStream(fis);
		
		while(((i=inStream.read()) != -1 ) && count != 4){
			c= (char) (i & 0xFF);		
			type+=String.valueOf(c);
			count++;		
		}
		inStream.close();
		return type;
	}
	
	public String getHash() {
		
		return hash;
	}
}
