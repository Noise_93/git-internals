package core;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitCommitObject {

	private String path;
	private String commit;
	
	public GitCommitObject(String repoPath,String commitHash) {
		
		path=repoPath;
		commit=commitHash;
		
	}
	
	public String getHash() throws IOException {
		
		GitRepository repository=new GitRepository(path);
		return repository.getRefHash("refs/heads/master");
		
	}
	
	public String getTreeHash() throws FileNotFoundException, IOException {
		
		int i;
		char c;
		int count;
		String tree="";
		String[] dir= { path, commit.substring(0,2), commit.substring(2) };
		String file=path+"/objects/"+dir[1]+"/"+dir[2];
		FileInputStream fis = new FileInputStream(file);
		InflaterInputStream inStream=new InflaterInputStream(fis);
		
		for(count=0;count < 16 ; count++)
			inStream.read();
		count = 0;
		while(((i=inStream.read()) != -1) && count < 40 ){
			c= (char) (i & 0xFF);		
			tree+=String.valueOf(c);
			count++;
		}
		return tree;
		
	}
	
	public String getParentHash() throws FileNotFoundException, IOException {
		
		int i;
		char c;
		String parent="";
		String[] dir= { path, commit.substring(0,2), commit.substring(2) };
		String file=path+"/objects/"+dir[1]+"/"+dir[2];
		FileInputStream fis = new FileInputStream(file);
		InflaterInputStream inStream=new InflaterInputStream(fis);

		while(((i=inStream.read()) != -1)){
			c= (char) (i & 0xFF);		
			parent+=String.valueOf(c);					
		}
		
		String[] tree=parent.split("parent|author");
		String[] parentHash=tree[1].split(" |\n");
		
		return parentHash[1];

	}
	
	public String getAuthor() throws FileNotFoundException, IOException {
		
		int i;
		char c;
		String author="";
		String[] dir= { path, commit.substring(0,2), commit.substring(2) };
		String file=path+"/objects/"+dir[1]+"/"+dir[2];
		FileInputStream fis = new FileInputStream(file);
		InflaterInputStream inStream=new InflaterInputStream(fis);

		while(((i=inStream.read()) != -1)){
			c= (char) (i & 0xFF);		
			author+=String.valueOf(c);					
		}
		
		String[] tree=author.split("parent|author");
		String[] authorCommit=tree[2].split(" ");
		return authorCommit[1] + " " +authorCommit[2] + " " + authorCommit[3];
		
	}
	
}
