package core;
import java.io.*;

public class GitRepository {
	
	private String path;
	
	public GitRepository(String RepoPath){
	
		path=RepoPath;
	
	}
	
	public String getHeadRef() throws IOException {
		
		String toTokenize=null;
		File fileHEAD=new File("/home/vittima/git-internals/sample_repos/sample01/HEAD");
		FileInputStream fis=new FileInputStream(fileHEAD);
		BufferedReader br=new BufferedReader(new InputStreamReader(fis));    
		
		String line=null;
		while ((line=br.readLine()) != null) {
			toTokenize=line;
		}
		String[] head=toTokenize.split("\\s");
		return head[1];
	
	}
	
	public String getRefHash(String pathHead) throws IOException {

		String hash;
		File fileMaster= new File(path+"/"+pathHead);
		FileInputStream fis=new FileInputStream(fileMaster);
		BufferedReader br=new BufferedReader(new InputStreamReader(fis));    
		hash=br.readLine();
		return hash;
		
	}

}
