package core;

import java.io.*;
import java.util.function.IntPredicate;
import java.util.zip.InflaterInputStream;

public class GitBlobObject extends GitObject {

	private String refs;
	private String blob;
	
	public GitBlobObject(String path, String hash) {
		
		super(path,hash);
		refs=path;
		blob=hash;
	}
	
	public String getType() throws Exception{
		
		return super.getType();
	}
	
	public String getContent() throws FileNotFoundException, IOException {
		
		
		int i;
		char c;
		int count;
		String content="";
		String[] dir= { refs, blob.substring(0,2), blob.substring(2) };
		String file=refs+"/objects/"+dir[1]+"/"+dir[2];
		FileInputStream fis = new FileInputStream(file);
		InflaterInputStream inStream=new InflaterInputStream(fis);
		
		for(count=0;count < 8 ; count++)
			inStream.read();
		while((i=inStream.read()) != -1 ){
			c= (char) (i & 0xFF);		
			content+=String.valueOf(c);		
		}
		
		return content;
		
	}

	public String getHash() {
		
		return super.getHash();
	}

}
